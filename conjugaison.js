let conjugaison = function(verbe) {
    if (verbe.substring(verbe.length-2,verbe.length) == 'er')
    {
        verbe = verbe.substring(0, verbe.length-2);
        return "je " + verbe + "e\n" + "tu " + verbe + "es\n" + "il/elle " + verbe + "e\n" + "nous " + verbe + "ons\n" + "vous " + verbe + "ez\n" + "ils/elles " + verbe + "ent";
    } else {
        return "Ce n'est pas un verbe du premier groupe"
    }
}

module.exports = conjugaison;
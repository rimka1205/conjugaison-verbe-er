const conjugaison = require("./conjugaison");

test('Conjuge un verbe du premier groupe', () => {
    expect(conjugaison('marcher')).toBe('je marche\ntu marches\nil/elle marche\nnous marchons\nvous marchez\nils/elles marchent');
  });